# Gauge Krahe's dotfiles

![Current setup](https://i.imgur.com/HatMqWG.png)

**Distribution:** Arch (GNU/Linux)

**WM:** bspwm

**Terminal:** [st](https://gitlab.com/GaugeK/st)

**Font:** t kiwi Wide - Made by [github/turquoise-hexagon](https://github.com/turquoise-hexagon/)
