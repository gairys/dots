echo -en "\e]P022283c"  # black
echo -en "\e]P1f75e6f"  # darkred
echo -en "\e]P2a6dc7e"  # darkgreen
echo -en "\e]P3f5b68e"  # brown
echo -en "\e]P485acff"  # darkblue
echo -en "\e]P5dd99ee"  # darkmagenta
echo -en "\e]P680b9ff"  # darkcyan
echo -en "\e]P78888a0"  # lightgrey
echo -en "\e]P84c5376"  # darkgrey
echo -en "\e]P9f35a6b"  # red
echo -en "\e]PAa6dc7e"  # green
echo -en "\e]PBf5b68e"  # yellow
echo -en "\e]PC85acff"  # blue
echo -en "\e]PDdd99ee"  # magenta
echo -en "\e]PE80b9ff"  # cyan
echo -en "\e]PFcfcff9"  # white
