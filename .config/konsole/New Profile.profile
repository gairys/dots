[Appearance]
ColorScheme=Krayons
Font=Iosevka Term,11,-1,5,50,0,0,0,0,0,Regular

[Cursor Options]
CursorShape=2

[General]
Command=/bin/zsh
Name=New Profile
Parent=FALLBACK/
TerminalColumns=100
TerminalMargin=12
TerminalRows=26
